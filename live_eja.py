from flask import Flask, render_template, request
import pandas as pd

app = Flask(__name__)

@app.route('/')
def index():
    # Ler o arquivo Excel
    excel_path = r'C:\Users\paulo.goncalves\Documents\Python\Disponibilizacao\Site Live EJA\calendario_live_eja.xlsx'
    df = pd.read_excel(excel_path)

    # Obter as séries únicas para o campo selecionável
    series = df['Série'].unique()

    return render_template('index.html', series=series)

@app.route('/calendario/', methods=['GET', 'POST'])
def visualizar_calendario():
    serie = request.args.get('serie', None)

    if serie is not None:
        # Ler novamente o arquivo Excel para obter os detalhes do calendário
        df = pd.read_excel(r'C:\Users\paulo.goncalves\Documents\Python\Disponibilizacao\Site Live EJA\calendario_live_eja.xlsx')

        # Filtrar dados para a série selecionada
        dados_serie = df[df['Série'] == serie]

        # Obter a data atual para comparação
        data_atual = pd.to_datetime('today')

        # Adicionar uma nova coluna para armazenar o link correto
        dados_serie['Link_Correto'] = dados_serie.apply(
            lambda row: row['Link_Gravado'] if pd.notnull(row['Link_Gravado']) and pd.to_datetime(row['Data']).date() < data_atual.date() else row['Link'],
            axis=1
        )

        # Filtrar apenas os eventos com link disponível
        eventos = []
        for _, evento in dados_serie.iterrows():
            start_datetime = pd.to_datetime(str(evento['Data']) + ' ' + str(evento['Horário']))
            is_today_event = start_datetime.date() == data_atual.date()
            eventos.append({
                'title': evento['Disciplina'],
                'start': start_datetime.strftime('%Y-%m-%d %H:%M:%S'),
                'url': evento['Link_Correto'],
                'is_today_event': is_today_event
            })

        if eventos:
            print(f"Eventos para {serie}: {eventos}")
            return render_template('calendario.html', serie=serie, eventos=eventos)
        else:
            print(f"Nenhum evento disponível para {serie}")
            return render_template('sem_calendario.html', serie=serie)

    print("Série não especificada")
    return render_template('sem_calendario.html', serie=None)

@app.route('/faq/')
def faq():
    return render_template('faq.html')  # Certifique-se de ter o template FAQ

if __name__ == '__main__':
    app.run(debug=False)
